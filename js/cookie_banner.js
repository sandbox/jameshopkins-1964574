(function ($) {

  Drupal.behaviors.cookie_banner = {
    attach: function (context, settings) {
      var cookie_name = settings.cookie_banner.cookie_banner_name;
      var cookie_message = settings.cookie_banner.cookie_banner_message;
      // PHP time is expressed in seconds, JS needs milliseconds.
      var cookie_duration = settings.cookie_banner.cookie_banner_duration * 1000;

      $('body').prepend(cookie_message);
      
      Drupal.cookie_banner.closeBanner(cookie_name, cookie_duration);
      
    }
  };

  Drupal.cookie_banner = {};

  Drupal.cookie_banner.closeBanner = function(name, time) {
    if (document.cookie.indexOf(name) == -1) {
       Drupal.cookie_banner.setCookie(name, '1', time);
    } else{
       $('#cookie-banner').hide();
    }
      
    $('#cookie-banner .close').click(function() {
      $('#cookie-banner').hide();
    });
  }
  
  Drupal.cookie_banner.setCookie = function(name, value, time) {
    var expires = "";
    if (time) {
      var date = new Date();
      date.setTime(time);
      expires = "; expires=" + date.toGMTString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
  };

})(jQuery);