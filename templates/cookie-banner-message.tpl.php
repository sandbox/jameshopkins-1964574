<div id="cookie-banner">
  <span class="close">Close</span>
  <p>
    <?php print $use_cookie_message; ?>
    <a href="<?php print $more_info_url; ?>"><?php print $more_info_message; ?></a>
  </p>
</div>
